# なにこれ

Zola と Tailwind CSS で作成するコーポレートページのリポジトリ。

## 使い方

### 開発時

Tailwind CSS の継続的なリフレッシュ処理

```bash
npx tailwindcss -i style.css -o static/style.css --watch
```

### リリース時

```bash
# Tailwind CSS のビルド処理
npx tailwindcss -i style.css -o static/style.css --minify

# Zola のビルド処理
zola build
```
